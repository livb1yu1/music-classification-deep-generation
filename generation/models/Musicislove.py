'''Working copy, please don't use this one'''

import os
import matplotlib.pyplot as plt
import numpy as np
import types
import pydot
from keras.layers import *
from keras import Model

# from models.MuseGAN import MuseGAN
from utils.loaders import load_music

from music21 import midi
from music21 import note, stream, duration

class Musicislove():
    def __init__(self
             , input_dim
             , discrim_learning_rate
             , gen_learning_rate
             , opt
             , grad_weight
             , z_dim
             , batch_size
             , tracks
             , bars
             , steps_
             , pitches
             , randomMean
             , randomStdDev):

        self.name = 'dankmemes'
        self.input_dim = input_dim
        self.discrim_learning_rate = discrim_learning_rate
        self.gen_learning_rate = gen_learning_rate
        self.opt = opt
        self.grad_weight = grad_weight
        self.z_dim = z_dim
        self.batch_size = batch_size
        self.tracks = tracks
        self.bars = bars
        self.steps_ = steps_
        self.pitches = pitches
        self.initial_weights = np.random.normal(randomMean, randomStdDev)
        self.grad_weight = grad_weight
        self.batch_size = batch_size

        #todo add d and g losses

        self.epoch_num = 0

    def discriminator_setup(self):
        inputLayer = Input(self.input_dim, name = self.name + '_disc')
        x = Conv3D(
            strides=(1,1,1),
            padding="valid",
            filters=128,
            kernel_size=(2,1,1),
            activation="tanh"
        )(inputLayer)
        x = Conv3D(
            strides=(1,1,1),
            padding="valid",
            filters=128,
            kernel_size= self.bars - (2,1,1),
            activation="tanh"
        )(x)
        x = Conv3D(
            strides=(1,1,12),
            padding="same",
            filters=128,
            kernel_size= (1,1,12),
            activation="tanh"
        )(x)
        x = Conv3D(
            strides=(1,1,7),
            padding="same",
            filters=128,
            kernel_size= (1,1,7),
            activation="tanh"
        )(x)
        x = Conv3D(
            strides=(1,2,1),
            padding="same",
            filters=128,
            kernel_size= (1,2,1),
            activation="tanh"
        )(x)
        x = Conv3D(
            strides=(1,2,1),
            padding="same",
            filters=128,
            kernel_size= (1,2,1),
            activation="tanh"
        )(x)
        x = Conv3D(
            strides=(1,2,1),
            padding="same",
            filters=256,
            kernel_size= (1,4,1),
            activation="tanh"
        )(x)
        x = Conv3D(
            strides=(1,2,1),
            padding="same",
            filters=512,
            kernel_size= (1,3,1),
            activation="tanh"
        )(x)
        x = Flatten(x)
        x = Dense(1024, kernel_initializer=self.initial_weights)
        x = LeakyReLU()(x)

        self.disc = Model(inputLayer, Dense(1, activation=None, kernel_initializer = self.weight_init)(x))

    def conv_t(self, x, f, k, s, a, p, bn):
        x = Conv2DTranspose(
            filters=f
            , kernel_size=k
            , padding=p
            , strides=s
            , kernel_initializer=self.weight_init
        )(x)

        if bn:
            x = BatchNormalization(momentum=0.9)(x)

        if a == 'relu':
            x = Activation(a)(x)
        elif a == 'lrelu':
            x = LeakyReLU()(x)

        return x

    def gen_setup(self):
        chords_input = Input(shape=(self.z_dim,), name='chords_input')
        style_input = Input(shape=(self.z_dim,), name='style_input')
        melody_input = Input(shape=(self.tracks, self.z_dim), name='melody_input')
        groove_input = Input(shape=(self.tracks, self.z_dim), name='groove_input')

        temporal_input = Input(shape=(self.z_dim,), name='temporal_input')

        x = Reshape([1, 1, self.z_dim])(temporal_input)
        x = self.conv_t(x, f=1024, k=(2, 1), s=(1, 1), a='lrelu', p='valid', bn=True)
        x = self.conv_t(x, f=self.z_dim, k=(self.n_bars - 1, 1), s=(1, 1), a='relu', p='valid', bn=True)

        output_layer = Reshape([self.n_bars, self.z_dim])(x)
        chords_over_time = Model(temporal_input,output_layer)

        melody_over_time = [None]*self.tracks
        self.melody_tempNetwork = [None]*self.tracks
        for i in range(self.tracks):
            self.melody_tempNetwork[i] = Model(temporal_input,output_layer)












